#README

## Resources

* [Safari OnLine Functional Programming PlayList](https://www.safaribooksonline.com/playlists/b10758bb-4e8c-481d-ac43-d3db3bec6721/) 
* [Slack Workspace: Java Academy](https://slack.com/signin) 
    + Sign in **JACAD** workspace
    + Provide the email used in the sign in to request access to the instructor of the course
    + Ensure you are in the **fp-java8** channel.  

### Session 1
#### Lectures for Preparation
   * Java 101: Functional Programming for Java developers
        + [Part 1](https://www.javaworld.com/article/3314640/java-101-functional-programming-for-java-developers-part-1.html)
        + [Part 2](https://www.javaworld.com/article/3314640/java-101-functional-programming-for-java-developers-part-2.html)
   * [Functional Programming in Java: How functional techniques improve your Java programs](https://learning.oreilly.com/library/view/functional-programming-in/9781617292736)
        + [Chapter I](https://learning.oreilly.com/library/view/functional-programming-in/9781617292736/kindle_split_008.html) 
   * [Beginning Java 8 Language Features: Lambda Expressions, Inner Classes, Th reads, I/O, Collections,and Streams](https://learning.oreilly.com/library/view/beginning-java-8/9781430266594/)
        + [Chapter 5](https://learning.oreilly.com/library/view/beginning-java-8/9781430266594/9781430266587_Ch05.xhtml)
            - Lambda Expressions
            - Functional Interface
            - Method References 
            
#### Practice

   1. *Exercises 01* - There are 4 exercises to do, check the requirements and rise any doubt 
   (A couple of the exercises will be used as demo)
        - org.jacademy.sesion01.exercise.Exercises01
        - org.jacademy.sesion01.exercise.Exercises01Test
   1. *Exercises 02* - There are 3 exercises to do, check the requirements and rise any doubt.
        - org.jacademy.sesion01.exercise.Exercises02
        - org.jacademy.sesion01.exercise.Exercises02Test
    
   NOTES:
   - Solutions will be received until Thursday 19, 5:00 p.m.
   - Solutions will be published before session 02.


### Further Reading
   * [Why is the raising of an exception a side effect?](https://stackoverflow.com/questions/10703232/why-is-the-raising-of-an-exception-a-side-effect)
   * [5 kinds of side-effects](https://medium.com/@igorwojda/5-kinds-of-side-effects-a67f6b495af9)
   * [Effective Java, 3rd Edition](https://learning.oreilly.com/library/view/Effective+Java,+3rd+Edition/9780134686097)
        - ITEM 17: Minimize Mutability
        - ITEM 42: Prefer Lambdas to Anonymous Classes
        - ITEM 43: Prefer Method references to Lambdas
        - ITEM 44: Favor the use of Standard Functional Interfaces
        - ITEM 46: Prefer Side-Effect-Free functions in streams
  
### Session 2
#### Lectures for Preparation
   * [Functional Programming in Java: How functional techniques improve your Java programs](https://learning.oreilly.com/library/view/functional-programming-in/9781617292736)
       + [Chapter II](https://learning.oreilly.com/library/view/functional-programming-in/9781617292736/kindle_split_009.html) 
       + [Chapter VI](https://learning.oreilly.com/library/view/functional-programming-in/9781617292736/kindle_split_013.html)
   * [Getting started with Java Monads](http://mrbool.com/getting-started-with-java-monads/34302)
   * Monads for Java Developers
       + [Part 1](https://medium.com/@afcastano/monads-for-java-developers-part-1-the-optional-monad-aa6e797b8a6e)
       + [Part 2](https://medium.com/@afcastano/monads-for-java-developers-part-2-the-result-and-log-monads-a9ecc0f231bb)
   * [Does JDK8's Optional Class satisfy the monad Laws? Yes, it Does](https://devblog.timgroup.com/2013/11/11/does-jdk8s-optional-class-satisfy-the-monad-laws-yes-it-does/)
  
#### Practice
 
 
   1. Exercises 01 - There are 3 exercises to do, check the requirements and rise any doubt 
         - org.jacademy.session02.exercise.Exercise01
            - From the sesion01.Exercise01.c1, create a curried function to calculate the tax.
            - org.jacademy.sesion01.exercise.Exercises02, create a curried function to generate the Address from an String
         - org.jacademy.sesion01.exercise.Exercises01Test
   1. Exercises 02 - There are 3 exercises to do, check the requirements and rise any doubt.
         - org.jacademy.sesion01.exercise.Exercises02
         - org.jacademy.sesion01.exercise.Exercises02Test
     
   NOTES:
    - Solutions will be received until Thursday 29, 5:00 p.m.
    - Solutions will be published after session 02.
    
### Session 3
#### Lectures for Preparation

 

   * [Beginning Java 8 Language Features: Lambda Expressions, Inner Classes, Th reads, I/O, Collections,and Streams](https://learning.oreilly.com/library/view/beginning-java-8/9781430266594/)
    - [Chapter 13 - Streams](https://learning.oreilly.com/library/view/beginning-java-8/9781430266594/9781430266587_Ch13.xhtml)
   * [Package java.util.function - Summary](https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html)
        
# Tools & tech-stack
* [Java 8](https://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html)
* [JUnit 4.2](https://www.javaguides.net/p/junit-4.html)
* [JUnit Data Provider](https://github.com/TNG/junit-dataprovider)
* [git - Atlassian Tutorials](https://www.atlassian.com/git/tutorials)
* [Maven - Starter Guide](https://maven.apache.org/guides/getting-started/index.html)
* [IntelliJ Idea](https://www.jetbrains.com/help/idea/install-and-set-up-product.html)

### 
            
## Common Maven commands
### Build the project
```
mvn clean compile
```

### Run all the tests

```
mvn test

```


###Run specific tests
```
mvn test -Dtest=EvaluationSampleTest
```

- Replace 'EvaluationSampleTest' by the name of the class containing the tests to execute.
- Add # followed by the name of the method test to specify a single method test to execute.

```
mvn test -Dtest=EvaluationSampleTest#getCountryLazy
```


---


## Common git commands

### Clone the repository

`git clone https://<username>@bitbucket.org/jacad/fpc-java8.git`

### Refresh to master
`git pull origin master`

### Create a new branch to work with
`git branch -m <username>/Exercises-Solutions-Session01"`

### Add all the changes
`git add -A`

### Commit the changes to the local repository
`git commit -m "Solutions for Exercise 01"`

### Deliver the changes to the remote repository
`git push origin <username>/Exercises-Solutions-Session01"`
