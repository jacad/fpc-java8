package org.jacademy.session02.exercise;

import org.jacademy.model.Address;
import org.jacademy.model.City;
import org.jacademy.model.Country;
import org.jacademy.model.Person;
import org.jacademy.model.State;
import org.jacademy.model.shop.Product;
import org.jacademy.model.shop.PurchaseDetail;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.EnumMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static org.jacademy.model.shop.Product.ProductType.Food;
import static org.jacademy.model.shop.Product.ProductType.Liqueur;
import static org.jacademy.model.shop.Product.ProductType.Medicine;
import static org.jacademy.model.shop.Product.ProductType.Other;
import static org.jacademy.model.shop.Product.ProductType.Tobacco;

class Exercise02 {

    private final static Map<Product.ProductType, Double> TAX_TABLE = new EnumMap<>(Product.ProductType.class);
    static {
        TAX_TABLE.put(Tobacco, 0.19);
        TAX_TABLE.put(Liqueur, 0.22);
        TAX_TABLE.put(Food, 0.0);
        TAX_TABLE.put(Medicine, 0.0);
        TAX_TABLE.put(Other, 0.16);
    }
    /*
    a) Using Optional, given a possible null Person, get the default language of the country in the address.

    If any of the values is missing, return "Not available"

    TIP: Use the Locale.getDisplayLanguage to get the language.
     */

    String getDisplayNameIn(Person person) {
        return Optional.ofNullable(person)
                   .map(Person::getAddress)
                   .map(Address::getCity)
                   .map(City::getState)
                   .map(State::getCountry)
                   .map(Country::getIsoCode)
                   .flatMap(this::getLocale)
                   .map(Locale::getDisplayLanguage)
                   .orElse("Not available");
    }

    /*
    b) Using Optional, given a possible null Person, get the age, but only if the person is male.
    Return 0 in any other case.
     */

    Long calculateAge(Person person) {
        return Optional.ofNullable(person)
                   .filter(p -> person.getSex() == Person.Sex.Male)
                   .map(Person::getBirthday)
                   .map(birthday -> ChronoUnit.YEARS.between(birthday, LocalDate.now()))
                   .orElse(0L);
    }

    /*
    c) Given two possible null purchase details, return the subtotal (sum of both), considering price and quantity;
    including the tax for each one.
    At the end, if the sum of both exceeds 1000. Apply a 5% discount.
     */
    Double calculateTotal(PurchaseDetail p1, PurchaseDetail p2) {
        Optional<PurchaseDetail> optP2 = Optional.ofNullable(p2);
        return Optional.ofNullable(p1)
                   .flatMap(calculateSubtotalWithTaxes)
                   .flatMap(subtotal1 -> optP2
                                             .flatMap(calculateSubtotalWithTaxes)
                                             .flatMap(subtotal2 -> Optional.of(subtotal1 + subtotal2)))
                   .map(total -> total > 1000 ? total * 0.95 : total)
                   .orElse(Double.NaN);
    }

    private Function<PurchaseDetail, Optional<Double>> retrievePrice =
        purchaseDetail ->Optional.ofNullable(purchaseDetail)
                             .filter(pd -> pd.getQuantity() > 0)
                             .map(PurchaseDetail::getProduct)
                             .map(Product::getPrice)
                             .filter(price -> price > 0.0);

    private Function<PurchaseDetail, Function<Double, Optional<Double>>> calculateSubtotal =
        purchaseDetail -> price -> Optional.of(purchaseDetail.getQuantity() * price);

    private Function<PurchaseDetail, Function<Double, Optional<Double>>> calculatePriceWithTaxes =
        purchaseDetail -> subtotal -> Optional.ofNullable(purchaseDetail.getProduct())
                                          .map(p -> TAX_TABLE.getOrDefault(p.getType(), 0.16))
                                          .map(tax -> (1 + tax) * subtotal);

    Function<PurchaseDetail, Optional<Double>> calculateSubtotalWithTaxes = detail ->
                                                                                retrievePrice.apply(detail)
                                                                                    .flatMap(price -> calculateSubtotal.apply(detail).apply(price))
                                                                                    .flatMap(subtotal -> calculatePriceWithTaxes.apply(detail).apply(subtotal));

    private Optional<Locale> getLocale(String countryIsoCode) {
        Locale result = null;
        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.getCountry().equalsIgnoreCase(countryIsoCode)) {
                result = locale;
                break;
            }
        }
        return Optional.ofNullable(result);
    }
}
