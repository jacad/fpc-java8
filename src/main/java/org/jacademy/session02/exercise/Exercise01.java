package org.jacademy.session02.exercise;

import org.jacademy.model.Person;

import java.time.LocalDate;
import java.util.function.Function;

public class Exercise01 {
    /*
    a) Create a curried function to calculate the final velocity given:
     - Initial Velocity (vi)
     - Acceleration
     - Time
     vf= vi+a*t
     */
    Function<Double, Function<Double, Function<Double, Double>>> finalVelocity = vi -> a -> t -> vi + a*t;

    /*
    b) Create a curried function, that received the subtotal and discount percentage and calculates the total price after discount.
     */
    Function<Double, Function<Double, Double>> calculateDiscount = subtotal -> discount -> subtotal * (1 - discount);

    /*
    c) Create a curried function to create a Person object receiving: name, birthday and sex; all the values will be passed as String.

   Example:

   Juan Perez, 1985-06-22, Male.
   If a value is incorrect or missing, leave it as null.

   The Birthday is in ISO
   For this exercise ignore the lastName & address fields.
    */
    Function<String, Function<String, Function<String, Person>>> convertStrToPerson =
        name -> birthdayStr -> {
            LocalDate birthday = safeDateParsing(birthdayStr, null);
            return sexStr -> new Person(name, birthday, safeSexParsing(sexStr));
        };

    private Person.Sex safeSexParsing(String sexStr) {
        Person.Sex sex;
        try {
            sex = Person.Sex.valueOf(sexStr);
        } catch (Exception e) {
            sex = null;
        }
        return sex;
    }

    private LocalDate safeDateParsing(String dateStr, LocalDate defaultValue) {
        LocalDate birthday;
        try {
            birthday = LocalDate.parse(dateStr);
        } catch (Exception e) {
            birthday = defaultValue;
        }
        return birthday;
    }
}
