package org.jacademy.session02.sample;

import org.jacademy.utils.GeneratorUtils;

import java.util.Optional;

public class _02a_OptionalBasics {

    public static void main(String... args) {
        _02a_OptionalBasics optionalBasics = new _02a_OptionalBasics();

        optionalBasics.creatingOptional();
        optionalBasics.mapVsFlatMap();
        optionalBasics.filtering();
        optionalBasics.ifPresent();
        optionalBasics.retrievingInformation();
        optionalBasics.throwingException();
    }

    //Invert the declaration for opt2 and opt3
    void creatingOptional() {
        Optional<Integer> opt1 = Optional.empty();
        Optional<Integer> opt2 = Optional.of(1);
        Optional<Integer> opt3 = Optional.ofNullable(null);
     }

     void mapVsFlatMap() {
        System.out.println("\n\nmap vs flatMap");
        Optional<String> opt1 = Optional.of("test");

        System.out.println("\tApplying map to non-optional result function: " + opt1.map(this::reverse));
        System.out.println("\tApplying flatMap to optional result function: " + opt1.flatMap(this::getIfMatched));

        System.out.println("\tApplying map to optional result function: " + opt1.map(this::getIfMatched));
        //System.out.println("\tApplying flatMap to non-optional result function: " + opt1.flatMap(this::reverse));
     }

    void filtering() {
        System.out.println("\n\nFiltering");
        Optional<String> opt1 = Optional.of("test").filter(str -> str.length() > 5);
        Optional<String> opt2 = Optional.of("Another test").filter(str -> str.length() > 5);

        System.out.println("\tOpt 1: " + opt1);
        System.out.println("\tOpt 2: " + opt2);
    }

    void ifPresent() {
        System.out.println("\n\nifPresent");
        Optional.of("test").filter(str -> str.length() > 5)
            .ifPresent(System.out::println);
        Optional.of("Another test").filter(str -> str.length() > 5)
            .ifPresent(System.out::println);

    }

    void retrievingInformation() {
        System.out.println("\n\nRetrieving information:");
        Optional<Integer> opt1 = Optional.of("1234")
                                        .filter(str -> str.matches("\\d+"))
                                        .map(Integer::parseInt);
        Integer result1a = opt1.get();
        Integer result1b = opt1.isPresent() ? opt1.get() : GeneratorUtils.generateRandomNumber();
        System.out.printf("\tResult 1a [is present: %b] : %s\n", opt1.isPresent(), result1a);
        System.out.printf("\tResult 1b [is present: %b] : %s\n", opt1.isPresent(), result1b);

        Optional<Integer> opt2 = Optional.of("test")
                                     .filter(str -> str.matches("\\d+"))
                                     .map(Integer::parseInt);
        Integer result2a = opt2.orElse(GeneratorUtils.generateRandomNumber());
        Integer result2b = opt2.isPresent() ? opt2.get() : GeneratorUtils.generateRandomNumber();
        System.out.printf("\tResult 2a [is present: %b] : %s\n", opt2.isPresent(), result2a);
        System.out.printf("\tResult 2b [is present: %b] : %s\n", opt2.isPresent(), result2b);

        Optional<Integer> opt3 = Optional.of("test").filter(str -> str.matches("\\d+"))
                                     .map(Integer::parseInt);
        Integer result3 = opt3.orElseGet(GeneratorUtils::generateRandomNumber);
        System.out.printf("\tResult 3 [is present: %b] : %s\n", opt3.isPresent(), result3);


    }

    void throwingException() {
        System.out.println("\n\nThrowing Exception:");
        Optional<Integer> opt1 = Optional.of("1234")
                                     .filter(str -> str.matches("\\d+"))
                                     .map(Integer::parseInt);

        Optional<Integer> opt2 = Optional.of("test")
                                     .filter(str -> str.matches("\\d+"))
                                     .map(Integer::parseInt);
        try {
            Integer res1 = opt1.orElseThrow(() -> new Exception("The optional 1 is empty"));
            System.out.println(res1);
            System.out.println("\tResult 1: " + res1);

            Integer res2 = opt2.orElseThrow(() -> new Exception("The optional 2 is empty"));
            System.out.println("\tResult 2: " + res2);
        } catch (Exception e) {
            System.err.printf("\t\tException: %s", e.getMessage());
        }
    }

    private String reverse(String original) {
        return new StringBuilder(original).reverse().toString();
    }

    Optional<String> getIfMatched(String original) {
        return original.matches("(\\d\\w\\d)+")
                   ? Optional.of(original)
                   : Optional.empty();
    }

}
