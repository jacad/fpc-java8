package org.jacademy.session02.sample;

import org.jacademy.model.City;
import org.jacademy.model.Country;
import org.jacademy.model.State;

import java.util.Locale;
import java.util.function.Function;

class _01b_CurryingUsage {
    public static void main(String... args) {
        _01b_CurryingUsage currying = new _01b_CurryingUsage();
        currying.executeAll();
    }

    private Function<String, Function<String, Function<String, City>>> cityGenerator =
        isoCode -> {
            Country country = getCountry(isoCode);
            System.out.println("-Country generated in 1st function: " + country);
            return stateName -> {
                State state = new State(stateName, country);
                System.out.println("--State generated in 2nd function: " + state);
                return cityName -> {
                    City city = new City(cityName, state);
                    System.out.println("---City generated in 3rd function: " + city);
                    return city;
                };
            };
        };

    private void executeAll() {
        String[] values = "Guadalajara,Jalisco,MX".split(",");

        Function<String, Function<String, City>> cityGeneratorForMX = cityGenerator.apply(values[2]);
        Function<String, City> cityGeneratorForJalisco = cityGeneratorForMX.apply(values[1]);

        City city1 = cityGeneratorForJalisco.apply(values[0]);
        System.out.println("City 1:\t" + city1);

        City city2 = cityGeneratorForJalisco.apply("Zapopan");
        System.out.println("City 2:\t" + city2);

        Function<String, City> cityGeneratorForAgs = cityGeneratorForMX.apply("Aguascalientes");
        City city3 = cityGeneratorForAgs.apply("Aguascalientes");
        System.out.println("City 3:\t" + city3);

    }

    private Country getCountry(String isoCode) {
        return new Country(new Locale("", isoCode).getDisplayName(), isoCode);
    }
}
