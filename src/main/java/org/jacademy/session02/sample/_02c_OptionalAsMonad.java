package org.jacademy.session02.sample;

import java.util.Optional;

class _02c_OptionalAsMonad {

    //NOTES:
    // 1) Pass Optional as input parameters is a bad practice.
    // 2) Define instance or class variables as Optional is bad practice
    // 3) Return Optional is acceptable, while it is not overused.
    Optional<Integer> optionalAdd(Integer a, Integer b) {
        Optional<Integer> x = Optional.ofNullable(a);
        Optional<Integer> y = Optional.ofNullable(b);

        //_x and _y are the wrapped values.
        return x.flatMap(
            _x -> y.flatMap(
                _y -> Optional.of(_x + _y)
            )
        );
    }
}
