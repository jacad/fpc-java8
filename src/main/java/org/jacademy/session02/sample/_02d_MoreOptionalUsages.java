package org.jacademy.session02.sample;

import org.jacademy.model.shop.Product;
import org.jacademy.model.shop.PurchaseDetail;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static org.jacademy.model.shop.Product.ProductType.Food;
import static org.jacademy.model.shop.Product.ProductType.Liqueur;
import static org.jacademy.model.shop.Product.ProductType.Medicine;
import static org.jacademy.model.shop.Product.ProductType.Other;
import static org.jacademy.model.shop.Product.ProductType.Tobacco;

class _02d_MoreOptionalUsages {

    private final static Map<Product.ProductType, Double> TAX_TABLE = new EnumMap<>(Product.ProductType.class);
    static {
        TAX_TABLE.put(Tobacco, 0.19);
        TAX_TABLE.put(Liqueur, 0.22);
        TAX_TABLE.put(Food, 0.0);
        TAX_TABLE.put(Medicine, 0.0);
        TAX_TABLE.put(Other, 0.16);
    }

    private Function<PurchaseDetail, Optional<Double>> retrievePrice =
        purchaseDetail -> Optional.ofNullable(purchaseDetail)
                              .filter(pd -> pd.getQuantity() > 0)
                              .map(PurchaseDetail::getProduct)
                              .map(Product::getPrice)
                              .filter(price -> price > 0.0);

    private Function<PurchaseDetail, Function<Double, Optional<Double>>> calculateSubtotal =
        purchaseDetail -> price -> Optional.of(purchaseDetail.getQuantity() * price);

    private Function<PurchaseDetail, Function<Double, Optional<Double>>> calculateTax =
        purchaseDetail -> subtotal -> Optional.ofNullable(purchaseDetail.getProduct())
                              .map(p -> TAX_TABLE.getOrDefault(p.getType(), 0.16))
                              .map(tax -> tax * subtotal);

    String getTaxAsString(PurchaseDetail purchaseDetail) {

        return Optional.ofNullable(purchaseDetail)
            .filter(detail -> detail.getQuantity() > 0)
            .flatMap(detail ->
                    retrievePrice.apply(detail)
                        .flatMap(price -> calculateSubtotal.apply(detail).apply(price))
                        .flatMap(subtotal -> calculateTax.apply(detail).apply(subtotal)))
            .map(value -> String.format("Tax applied: $%5.2f", value))
            .orElse("Not enough information to calculate tax.");
    }
}
