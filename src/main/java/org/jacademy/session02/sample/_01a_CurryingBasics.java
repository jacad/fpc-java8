package org.jacademy.session02.sample;

import org.jacademy.model.City;
import org.jacademy.model.Country;
import org.jacademy.model.State;

import java.util.Locale;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

public class _01a_CurryingBasics {

    public static void main(String... args) {
        _01a_CurryingBasics currying = new _01a_CurryingBasics();

        System.out.println("f(2.5, 3.5) = " + currying.f.apply(2.5, 3.5));

        System.out.println("g(2.5)(3.5) = " + currying.g.apply(2.5).apply(3.5));

        Double[] sol = currying.secondGradeEq.apply(2.0).apply(6.0).apply(2.0);
        System.out.printf("\t\tx1 = %f,\n\t\tx2 = %f\n\n", sol[0], sol[1]);



        String[] values = "Guadalajara,Jalisco,MX".split(",");

        City aCity = currying.cityGenerator
                         .apply(values[2])
                         .apply(values[1])
                         .apply(values[0]);

        System.out.println("\nA city:\n\t" + aCity);

        City aComplexCity = currying.somethingMoreComplex
                                .apply(values[2], currying::getCountry)
                                .apply(values[1])
                                .apply(values[0]);

        System.out.println("\nA complex city:\n\t" + aComplexCity);
    }

    //2 argument function - BiFunction
    private BiFunction<Double, Double, Double> f = (x, y) -> x + Math.pow(y, 2);

    //Equivalent Carrying Function
    private Function<Double, Function<Double, Double>> g = x -> y -> x + Math.pow(y, 2);

    private Function<Double, Function<Double, Double>> g2 = x ->
                                                       (    y -> x + Math.pow(y, 2)     );

    Function<String, Function<String, Function<String, City>>> cityGenerator_Verbose = ((String isoCode) -> {
        return (
            (String stateName) -> {
            return (
                (String cityName) -> {
                    return new City(cityName, new State(stateName, getCountry(isoCode)));
                }
            );
        });
    });

    private Function<String, Function<String, Function<String, City>>> cityGenerator =
        isoCode ->
            stateName ->
                cityName ->
                    new City(cityName, new State(stateName, getCountry(isoCode)));


    private BiFunction<String, Function<String, Country>, Function<String, Function<String, City>>> somethingMoreComplex =
        (isoCode, countryFun) -> stateName -> cityName ->
            new City(cityName, new State(stateName, countryFun.apply(isoCode)));
    /*
    Create a curried function to solve 2nd Grade Equations
    x = ( -b ± √️(b^2 - 4*a*c) ) / 2*a
     */
    private Function<Double, Function<Double, Function<Double, Double[]>>> secondGradeEq = a -> b -> c ->
        new Double[] {
            (neg(b) + (Math.sqrt(sqr(b) - 4*a*c)) ) / (2*a),
            (neg(b) - (Math.sqrt(sqr(b) - 4*a*c)) ) / (2*a)
        };

    City parseCity(String locationLine) {
        return Optional.ofNullable(locationLine)
            .map(adr -> adr.split(","))
            .filter(values -> values.length >= 3)
            .map(values ->
                     cityGenerator.apply(values[2]).apply(values[1]).apply(values[0]))
//                     cityGenerator_Verbose.apply(values[2]).apply(values[1]).apply(values[0]))
            .orElseGet(() -> new City(null, null));
    }

    private Country getCountry(String isoCode) {
        return new Country(new Locale("", isoCode).getDisplayName(), isoCode);
    }

    private Double neg(Double val) {
        return val * -1;
    }

    private Double sqr(Double val) {
        return Math.pow(val, 2);
    }
}
