package org.jacademy.session03.sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class _01FunctionalInterface_Complement {
    List<String> processedElements = new ArrayList<>();

    Consumer<String> store = processedElements::add;

    Consumer<String> print = System.out::println;

    Supplier<Long> generator = () -> new Random().nextLong();

    Predicate<Long> isEven = number -> number % 2 == 0;

    Predicate<Long> isMultipleOf7 = number -> number % 7 == 0;

    Function<Long, Long> mod123 = number -> number % 7;

    Function<Long, Long> add14 = number -> number + 14;

    public static void main(String... args) {
        _01FunctionalInterface_Complement sample = new _01FunctionalInterface_Complement();
        sample.run();
    }

    private void run() {

        System.out.printf("mod123: %s\n", mod123.apply(723L));
        System.out.printf("Add14: %s\n", add14.apply(723L));
        System.out.printf("mod123 compose Add14: %s\n", mod123.compose(add14).apply(745L));
        System.out.printf("mod123 andThen Add14: %s\n", mod123.andThen(add14).apply(745L));
        System.out.printf("Add14 compose mod123: %s\n", add14.compose(mod123).apply(745L));
        System.out.printf("Add14 andThen mod123: %s\n", add14.andThen(mod123).apply(745L));

        for (int i = 0; i < 10; i ++) {
            Optional.ofNullable(generator.get())
                .filter(isEven.or(isMultipleOf7))
                .map(Object::toString)
                .ifPresent(store.andThen(print));
        }

        System.out.println(processedElements);
    }
}