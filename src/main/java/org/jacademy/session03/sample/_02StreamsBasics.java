package org.jacademy.session03.sample;

import org.jacademy.model.Country;

import java.util.Comparator;
import java.util.Locale;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

public class _02StreamsBasics {

    private final static Random RANDOM = new Random();

    public static void main(String... args) {
        new _02StreamsBasics().run();
    }

    private void run() {
        processASimpleStream();
        iteratedCreationWithSkipAndLimit();
        nontraversedAndtraversedStream();

        forEachExample();

        gettingOneElement();
        verifyMatching();
    }

    String source = "This is a simple string without special symbols or numbers";

    void processASimpleStream() {
        System.out.println("\n\nprocessASimpleStream");

        long count = Stream.of(source.split(" "))           //Initialize the stream
                         .filter(str -> str.length() > 2)          //Intermediate operation
                         .map(str -> str.replaceAll("i", "&"))
                         .peek(str -> System.out.printf("\t\tword: %s\n", str))
                         .count();                                 //Terminal operation=

        System.out.println(count);
    }

    void iteratedCreationWithSkipAndLimit() {
        System.out.println("\n\niteratedCreationWithSkipAndLimit");

        long total = Stream.iterate(1L, n -> n/2 + 3 * n )
                         .skip(5)
                         .peek(l -> System.out.printf("\t\tnumber: %d\n", l))
                         .limit(10)
                         .count();
        System.out.printf("\t Total of collected numbers: %d\n", total);
    }

    void nontraversedAndtraversedStream() {
        System.out.println("\n\nnontraversedStream");

        Stream<Country> countryStream = Stream.of(Locale.getAvailableLocales())
            .map(locale -> new Country(locale.getDisplayCountry(), locale.getCountry()))
            //.distinct()
            .peek(country -> System.out.println("\t\tCountry generated: " + country));

        System.out.println("\tThis is a stream non-traversed: " + countryStream.toString());

        long size  = countryStream.count();

        System.out.println("\tTotal of countries found " + size);
        //System.out.println("\tCountries found " + countryStream.toArray().length);
    }

    void gettingOneElement() {
        System.out.println("\n\ngettingOneElement");

        Optional<Country> aCountry = Stream.of(Locale.getAvailableLocales())
                                      .filter(locale -> !locale.getCountry().isEmpty())
                                      .map(locale -> new Country(locale.getDisplayCountry(), locale.getCountry()))

//                                      .sorted(Comparator.comparing(Country::getIsoCode).reversed())    //Sorting using comparator
//                                      .findAny();
//                                      .findFirst();
// Getting the longest/shortest name
                                      .min(Comparator.comparing(country -> country.getName().length())); // replace min/max

        aCountry.ifPresent(country -> System.out.printf("\t\t[%s] - %s", country.getIsoCode(), country.getName()));
    }

    void verifyMatching() {
        System.out.println("\n\nmatching");

        boolean aMatch = Stream.of(Locale.getAvailableLocales())
                        .filter(locale -> !locale.getCountry().isEmpty())
                        .map(locale -> new Country(locale.getDisplayCountry(), locale.getCountry()))
                        .map(Country::getName)
                        .noneMatch(name -> name.length() == 7); //replace for allMatch | noneMatch | anyMatch


        System.out.printf("\t\tWas there a match? %s\n", aMatch ? "Yes" : "No");
    }

    void  forEachExample() {
        System.out.println("\n\nFor each");
        RANDOM.ints(10).forEachOrdered(i -> System.out.println("\t\t Number: " + i)); //replace forEach
    }
}
