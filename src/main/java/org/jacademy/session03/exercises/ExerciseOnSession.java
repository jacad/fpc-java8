package org.jacademy.session03.exercises;

import org.jacademy.model.Country;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class ExerciseOnSession {

    //Generate a stream from using the current time in milliseconds as source,
    //Display only the exact those that are exact seconds (example: 1553356705000)
    //Limit the 'infinite' stream to N
    //peek the contents to see in the log the generated numbers
    long generatedCreationWithLimit(int n) {
        return Stream.generate(System::currentTimeMillis)
                   .filter(ms -> ms % 1000 == 0)
                   .peek(l -> System.out.printf("\t\tnumber: %d\n", l))
                   .limit(n)
                   .count();
    }

    //Create stream the sort all the composite isoCode - name length, note: <isoCode> - <country name>
    //Example: MX - Mexico
    //Length: 8, whitespaces and hyphen should not be counted.
    //and then alphabetically

    Object[] sortCountries(List<Country> countries) {
        Object[] objects = countries.stream()
                               .map(country -> country.getIsoCode() + " - " + country.getName())
                               .sorted(Comparator.comparing(str -> str.toString().length() - 3)
                                           .thenComparing(Object::toString))
                               .toArray();

        System.out.println(objects);

        return objects;
    }
}
