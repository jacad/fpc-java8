package org.jacademy.session03.exercises;

import org.jacademy.model.shop.Product;

import java.util.Comparator;
import java.util.List;

import static java.util.Objects.nonNull;

public class Exercise01 {

    //Giving a list of numbers as String, return a filtered list of those that are not multiple of any (2,3,5,7), greater or equal than 121 and lesser than 1333, sorted ascending.
    //The list must contain only unique numbers
    Object[] processNumber(List<String> input) {
        return input.stream()
            .map(Long::parseLong)
            .filter(n -> n > 121 && n < 1333)
            .filter(n -> !(n % 2 == 0 || n % 3 == 0 || n % 5 == 0 || n % 7 == 0))
            .distinct()
            .sorted()
            .toArray();
    }

    //Each line of the file contains:    //Givin a list of strings determinate, return a list of palindromes with
    // a length greater than 5 and sorted by length and then alphabetically
    Object[] findPalindromes(List<String> words) {
        return words.stream()
                   .filter(word -> word.length() > 5)
                   .filter(str -> (new StringBuilder(str).reverse()).toString().equalsIgnoreCase(str) )
                   .sorted(Comparator.comparing(String::length).thenComparing(Comparator.naturalOrder()))
                   .distinct()
                   .toArray();
    }

    // Giving a list of strings, containing:
    // <product-code>,<product-name>,<price>,<type>
    //0001,Coca-cola,12.30,Other
    //Independently of the input values, the output
    //1) No duplicated entries
    //2) If the input does not have enough information, it shouldn't be included output except for type,
    // it should be used Other as default
    //3) The output should be sorted by code
    Object[] readProducts(List<String> lines) {
        return lines.stream()
                   .filter(str -> nonNull(str) && !str.isEmpty())
                   .distinct()
                   .sorted()
                   .map(line -> line.split(","))
                   .filter(items -> items.length >= 4)
                   .filter(items -> !items[0].isEmpty() && !items[1].isEmpty())
                   .filter(items -> items[2].matches("\\d+\\.\\d+"))
                   .map(items -> new Product(items[0], items[1], Double.valueOf(items[2]), getProductType(items[3])))
                   .toArray();

    }

    private Product.ProductType getProductType(String str) {
        Product.ProductType type;
        try {
            type = Product.ProductType.valueOf(str);
        } catch (Exception e) {
            type = Product.ProductType.Other;
        }
        return type;
    }
}
