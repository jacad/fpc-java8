package org.jacademy.sesion01.exercise;

import org.jacademy.model.Address;
import org.jacademy.model.City;
import org.jacademy.model.Country;
import org.jacademy.model.State;

import java.util.HashMap;
import java.util.Locale;
import java.util.Optional;
import java.util.Map;
import java.util.StringJoiner;

import static java.util.Objects.nonNull;

class Exercises02 {

    /*
    a.1) In the constructor create a supplier that return all the Countries supported by Java.
    a.2) Replace the supplier by a Method Reference

     Tips:
    a) Locale.getISOCountries() // returns all the ISO Codes for the Countries supported by Java.
    b) new Locale("", isoCountry) // Creates a new Locale with country name.
    */
    private CountryService countryService = new CountryService(() -> {
        Map<String, Country> countriesByCode = new HashMap<>();

        for (String isoCountry : Locale.getISOCountries()) {
            System.out.println();
            Locale locale = new Locale("", isoCountry);
            Country country = new Country(locale.getDisplayName(), isoCountry);
            countriesByCode.put(isoCountry, country);
            System.out.println(country);
        }

        return countriesByCode;
    });

    private State defaultState = new State("Default State", countryService.getCountryByIsoCode("MX"));
    private int counter = 1;

    /**
     * Creates a city giving a line, which contains the follow values separated by comma:
     *  - City name,
     *  - State name,
     *  - Country ISO Code
     *
     *  Example:
     *  Guadalajara, Jalisco, MX
     *  City name: Guadalajara
     *  State name: Jalisco
     *  Country name: Mexico
     *
     *  If any of the values is missing, a mock city will be returned, this city will be in the form of:
     *
     *  null
     *      *  City name: Default City #1
     *      *  State name: Default State
     *      *  Country name: Mexico
     *
     * Subsequent cities must be #2, #3, etc.
     *
     * @param cityLine
     * @return City created from the given information. Note, if any of the elements is missing or the line is null,
     * a default city will be returned
     */
    /*
    Exercise:
    b.1) Add a predicate in a filter to avoid process lines with less than 3 items.
    b.2) Add a mapper function that receives an array of Strings and creates a new City using the values of that line.
    b.3) Create a supplier function to create new Default city with a counter
    b.4) Replace all the lambdas by Method References.
    b.5) Document the side-effects in the result functions.
    b.5.1) Document the drawbacks, risks and limitations caused by those side-effects.
     */
    City generateCityFromString(String cityLine) {
        return Optional.ofNullable(cityLine)
                   .map(line -> line.split(","))
                   .filter(values -> values.length >= 3)
                   .map(values -> {
                       Country country = countryService.getCountryByIsoCode(values[2].trim());
                       State state = new State(values[1].trim(), country);
                       return new City(values[0].trim(), state);
                   }).orElseGet(
                () -> new City("Default City #" + counter++, defaultState));
    }

    /**
     * Generates a string representing the address in the follow format:
     *
     * (line #1)
     * [line #2]
     * (city name), (state)
     * [country name]
     * Zip Code: (ZipCode)
     *
     * Output examples:
     *
     * Rafael Sanzio 150,
     * Zapopan, Jalisco
     * Zip Code: 40405
     *
     * line 2 and country name are optional, if they are missing (null or empty), the line is not included.
     * The rest of the parameters are mandatory, if missing, the entire address will be defaulted to: "Incomplete address"
     * @param address with the information to be used for the string.
     * @return address in the specified format.
     */
    /*
    Exercise 3:
    c.1) Add a lambda predicate for the filter to avoid process incomplete addresses.
    c.2) Add a lambda function mapper to convert from Address to String according to the rules.
    c.3) Replace both lambdas with Method references
     */
    String generateAddressString(Address address) {
        return Optional.ofNullable(address)
                   .filter(adr -> nonNull(adr.getCity())
                                      && nonNull(adr.getLine1())
                                      && nonNull(adr.getZipCode())
                                      && nonNull(adr.getCity().getState())
                                      && nonNull(adr.getCity().getName())
                   )
                   .map(adr -> {
                       StringJoiner sj = new StringJoiner("\n").add(adr.getLine1());
                       String line2 = adr.getLine2();
                       if (nonNull(line2)) {
                           sj.add(line2);
                       }
                       City city = adr.getCity();
                       State state = city.getState();
                       sj.add(city.getName() + ", " + state.getName());
                       Country country = state.getCountry();
                       if (nonNull(country)) {
                           sj.add(country.getName());
                       }
                       sj.add("Zip Code: " + adr.getZipCode());

                       return sj.toString();
                   })
                   .orElse("Incomplete address");
    }
}
