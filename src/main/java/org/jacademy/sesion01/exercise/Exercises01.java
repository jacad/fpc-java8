package org.jacademy.sesion01.exercise;

import org.jacademy.model.shop.Product;
import org.jacademy.model.shop.PurchaseDetail;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.Function;

class Exercises01 {

    private static final double DEFAULT_TAX = 0.16;
    private Map<Product.ProductType, Double> taxes = new EnumMap<>(Product.ProductType.class);

    Exercises01() {
        taxes.put(Product.ProductType.Food, 0.0);
        taxes.put(Product.ProductType.Liqueur, 0.22);
        taxes.put(Product.ProductType.Tobacco, 0.19);
        taxes.put(Product.ProductType.Other, DEFAULT_TAX);
        taxes.put(Product.ProductType.Medicine, 0.0);
    }

    /*
     * a) Creates a function that represents:
     * 3*x^2 - 6*x + 12.
     */
    Function<Double, Double> fx = x -> 3 * Math.pow(x, 2) - 6 * x + 12;

    /*
     * b) Define a function that calculates an age (difference in year between a date in the past and today).
     * Tip: Check ChronoUnit
     */
    Function<LocalDate, Long> calculateAge = date -> ChronoUnit.YEARS.between(date, LocalDate.now());

    /*
     * c.1) Define  function that calculates price plus tax for a purchase detail (product details and quantity),
     * according to the follow rules:
     * 1) Food & Medicine 0%
     * 2) Liqueur 22%
     * 3) Tobacco 19%
     * 4) ANY Other 16%
     */
    Function<PurchaseDetail, Double> calculatePriceWithTax = detail -> {
        Product product = detail.getProduct();
        double subtotal = product.getPrice() * detail.getQuantity();
        double tax = taxes.getOrDefault(product.getType(), DEFAULT_TAX);
        return subtotal * (1.0 + tax);
    };

    /*
     *  c.2) Re-do the previous, but using method reference instead of a lambda.
     */
    Function<PurchaseDetail, Double> calculatePriceWithTax_MethodReference = this::calculateTaxMethod;


    /*
     * d) Define a function using lambda that calculates the discount for a purchase, the discount is always 5% but only
     * for purchases greater than $1000.00.
     */
    Function<Double, Double> calculateDiscount = total -> total > 1000.00 ? total * 0.05 : 0.0;

    private Double calculateTaxMethod(PurchaseDetail detail) {
        Product product = detail.getProduct();
        double subtotal = product.getPrice() * detail.getQuantity();
        double tax = taxes.getOrDefault(product.getType(), DEFAULT_TAX);
        return subtotal * (1.0 + tax);
    }
}
