package org.jacademy.sesion01.sample;

import java.util.Optional;
import org.jacademy.model.City;
import org.jacademy.model.Country;
import org.jacademy.model.State;

class EvaluationSample {

    Country getCountryEager(City city) {
        System.out.println("Getting city's country or default - eager evaluation");

        return Optional.ofNullable(city)
                       .map(City::getState)
                       .map(State::getCountry)
                       .orElse(new Country("Default Eager :: Unknown"));
    }

    Country getCountryLazy(City city) {
        System.out.println("Getting city's country or default - lazy evaluation");
        return Optional.ofNullable(city)
                       .map(City::getState)
                       .map(State::getCountry)
                       .orElseGet(() -> new Country("Default Lazy :: Unknown"));
    }

}
