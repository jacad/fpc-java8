package org.jacademy.sesion01.sample;

import java.util.stream.Stream;
import org.jacademy.model.Country;
import org.jacademy.model.State;

class LambdaScopesSample {

    private static String classScopedVar = "(class)";

    private String instanceScopedVar = "(instance)";

    /*
    01 :: Scopes in Lambda:
    a) Class and Objects are accessible as in any other scope, no restrictions.
    b) Method variable are accessible, but they can not be modified.
    b.1) Un-comment finalMethodVar re-assignation
    b.2) Un-comment effFinalMethodVar re-assignation
    b.3) Un-comment effFinalMethodVar re-assignation
    c) Note that str can be modified inside the lambda, since it is a copy of the value and not
    the value itself.
     */
    void lambdaScopes() {
        String a = "(local::final::a)";
        final String b = "(local::final::b)";
        String effFinalInstanceVar = "(local::effectively final)";


        Stream.of(b, "(local::inline)")
            .forEach(str -> {

                //effFinalInstanceVar += "[modified inside lambda]"; //2.2

                str += "[modified inside lambda thru str]";
                //a += "[modified inside lambda directly]";    //2.1
                instanceScopedVar += " " + "[modified inside lambda directly]";
                classScopedVar += " " + "[modified inside lambda directly]";

                System.out.println("-----------------------------");
                System.out.println("1) " + str);
                System.out.println("2) " + classScopedVar);
                System.out.println("3) " + instanceScopedVar);
                System.out.println("4) " + effFinalInstanceVar);
                System.out.println("5) " + a);
                System.out.println("6) " + b);
                System.out.println("-----------------------------");
            });

        System.out.println("7) " + b);
        //effFinalInstanceVar = ""; //2.3
    }

    /*
    02 :: Causing Side-Effects even with final.
     */
    void lambdaWithSideEffects() {
        State jl = new State("Jalisco", null);
        State ags = new State("Aguascalientes", null);

        System.out.println(jl);
        System.out.println(ags);

        Stream.of(jl, ags).forEach(
            state -> state.setCountry(new Country("Mexico"))
        );

        System.out.println(jl);
        System.out.println(ags);
    }
}
