package org.jacademy.sesion01.sample;

import static org.jacademy.utils.GeneratorUtils.generateRandomNumbers;

import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

class LambdasInJava {

    /*
    01 :: Lambda using all the syntax elements.
     */
    BiFunction<Double, Double, Double> fullSyntax = (Double a, Double b) -> {
        double x = a + b;
        System.out.println(x);
        return x;
    };

    /*
    02 :: Lambda without data types the syntax elements.
     */
    BiFunction<Double, Double, Double> noTypes = (a, b) -> {
        double x = a + b;
        System.out.println(x);
        return x;
    };

    /*
    03 :: Lambda without data type and block (also, without return)
    */
    BiFunction<Double, Double, Double> simplifiedSyntax = (a, b) -> a + b;

    /*
    04 :: Lambda without data type and block (also, without return)
    */
    Function<Double, Double> simplifiedSyntaxOneArg =  x -> x*x / 2;

    /*
    05 :: Lambda No arguments
     */
    Supplier<Long> mySupplier = () -> System.nanoTime() - 1000000000;

    /*
    06 :: Lambda No  return type
    */
    Consumer<Long> myConsumer = x -> {
        System.out.println("The value received is: " + x);
    };

    /*
    07 ::
    - High Order Functions
    - Inline Lambda
     */
    void sampleWithSort() {

        List<Integer> testValues = generateRandomNumbers(10, 1000);
        System.out.println(testValues);

        Collections.sort(testValues,
            (a, b) -> a%2 == b%2 ? a.compareTo(b) : Integer.compare(a%2, b%2));

        System.out.println(testValues);

    }

    public static void main(String... args) {
        LambdasInJava l = new LambdasInJava();

        l.sampleWithSort();
    }
}
