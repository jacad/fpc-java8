package org.jacademy.sesion01.sample;
import java.util.function.Function;

class FunctionsInJava {

    /*
    01 :: Demo: a Function in Java is just an object.
     */
    private final Function<Double, Double> functionOldWay = new Function<Double, Double>() {
        @Override
        public Double apply(Double a) {
            System.out.println(
                "Class of myFunction: " + this.getClass().getName()
                    + "\n Is this an object  ? " + (this instanceof Object ? "yes" : "no")
            );
            return a * 2;
        }
    };

    /*
    02 :: Demo:
    1) Single Abstract Method
    2) Any default or static amount of methods.
    3) Un-comment method2 to see the compilation error.
     */
    @FunctionalInterface
    interface ExplicitFunctionalInterface {
        double method();
        //int method2();

        default long getTimestamp() {
            return System.currentTimeMillis();
        }

        static long getTimestampInNanoSeconds() {
            return System.nanoTime();
        }
    }

    /*
    03 :: Demo:
     - It is still a Functional Interface without the annotation, while there is a single
     abstract method.
     */
    interface ImplicitFunctionalInterface {
        double method();
    }

    ImplicitFunctionalInterface implicitFunctionalInterface =
        () -> System.currentTimeMillis() / 1000.0;

    /*
    04 :: Demo:
     - It is still not a Functional Interface without the annotation, if there are more than
     abstract method.
     */
    interface NotAFunctionalInterface {
        double method1();
        double method2();
    }


//    NotAFunctionalInterface notAFunctionalInterface =
//        () -> System.currentTimeMillis() / 1000.0;

    public static void main(String... args) {
        FunctionsInJava functionsInJava = new FunctionsInJava();
        functionsInJava.functionOldWay.apply(2.0);
    }

}
