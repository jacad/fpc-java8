package org.jacademy.sesion01.sample;

import static java.lang.String.format;

class MethodReferences {

    MethodReferences() {
        System.out.println("(constructor) ");
    }

    String getFromInstanceMethod(double number) {
        String result = "" + (++number / 2);
        System.out.println(format("(instance method) Input: [%f]; Output: [%s]", number, result));
        return result;
    }

    static String getFromClassMethod(String value) {
        String result = "result: " + (new StringBuilder(value)).reverse().toString();
        System.out.println(format("(class method) Input: [%s]; Output: [%s]", value, result));
        return result;
    }
}
