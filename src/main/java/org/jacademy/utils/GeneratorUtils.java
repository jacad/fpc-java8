package org.jacademy.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeneratorUtils {

    private static Random GENERATOR = new Random();

    private GeneratorUtils() {
    }

    public static List<Integer> generateRandomNumbers(int amountOfNumbers, int maxNumber) {
        List<Integer> result = new ArrayList<>();

        for (int i = 0; i < amountOfNumbers; i++) {
            result.add(GENERATOR.nextInt(10));
        }

        return result;
    }

    public static Integer generateRandomNumber() {
        int number = GENERATOR.nextInt();
        System.out.printf("\n[****Generating new random integer [%d]]\n", number);
        return number;
    }
}
