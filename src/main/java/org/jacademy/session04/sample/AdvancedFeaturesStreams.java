package org.jacademy.session04.sample;

import org.jacademy.model.Country;
import org.jacademy.model.shop.Product;
import org.jacademy.model.shop.Product.ProductType;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Double.parseDouble;
import static java.util.Arrays.stream;
import static java.util.Objects.nonNull;

public class AdvancedFeaturesStreams {

    public static void main(String... args) {
        AdvancedFeaturesStreams a = new AdvancedFeaturesStreams();
        a.usingFlatMap();

        a.grouping();
        a.reducing();


        a.intStreams();
    }

    private String contents = "001;Tequila Cuervo;235.355;Liqueur\n" +
                                  "002;Cafe;235.355;Food\n" +
                                  "003;Marlboro;47.89;Tobacco\n" +
                                  "004;Aspirina;17.22;Medicine\n" +
                                  "001;Tequila Cuervo;235.355;Liqueur\n";

    void usingFlatMap() {
        System.out.println("\n\nUsing flatMap:");
        Set<Product> products = Stream.of(contents)
                                   .map(c -> c.split("\n"))
                                   .flatMap(lines -> stream(lines)
                                                         .map(line -> line.split(";"))
                                                         .map(arrayToProduct))
                                   .collect(Collectors.toSet());

        System.out.println("\t\tSet of Products:" + products);
    }

    Function<String[], Product> arrayToProduct = values ->
        new Product(values[0], values[1], parseDouble(values[2]), ProductType.valueOf(values[3]));

    private Set<Country> countrySet = getCountrySet();

    Set<Country> getCountrySet() {
        return Stream.of(Locale.getAvailableLocales())
                   .filter(locale -> !locale.getDisplayCountry().isEmpty() || !locale.getCountry().isEmpty())
                   .map(locale -> new Country(locale.getDisplayCountry(), locale.getCountry()))
                   .collect(Collectors.toSet());
    }

    void grouping() {
        System.out.println("\n\nGrouping:");
        Map<Character, List<Country>> mapWithList =
            getCountrySet().stream().collect(
                Collectors.groupingBy(
                    country -> country.getName().charAt(0)
                ));

        System.out.println("\t\t Map default collection (list): " + mapWithList);

        Map<Character, Set<Country>> mapWithSet = getCountrySet().stream().collect(
            Collectors.groupingBy(
                country -> country.getName().charAt(0),
                Collectors.toSet()
            ));

        System.out.println("\t\t Map with Set: " + mapWithSet);

        TreeMap<Integer, Set<Country>> treeMapWithList = getCountrySet().stream().collect(
            Collectors.groupingBy(
                country -> country.getName().length(),
                TreeMap::new,
                Collectors.toSet()
            ));

        System.out.println("\t\t TreeMap: " + treeMapWithList);
    }

    void reducing() {
        System.out.println("\n\nReducing:");
        getCountrySet().stream()
            .map(Country::getIsoCode)
            .reduce((accumulated, newValue) -> accumulated + " | " + newValue)
            .ifPresent(result -> System.out.println("\t\tResult: " + result));

        BigInteger product = getCountrySet().stream()
                              .map(Country::getName)
                              .filter(str -> str.length() > 0)
                              .map(String::length)
                              .map(BigInteger::valueOf)
                              .reduce(BigInteger.ONE, BigInteger::multiply);

        System.out.println("\t\tProduct: " + product);

        BigInteger reduced = getCountrySet()
            .stream()
            .parallel()
            .map(Country::getName).reduce(
                BigInteger.ZERO,
                (num, str) -> {
                    BigInteger length = BigInteger.valueOf(str.length());
                    BigInteger accumulator = length.add(num);
                    System.out.println("\t\t\t--Calling accumulator Length of [" + str + "] + " + num + " = " + accumulator);
                    return accumulator;
                },
                (a, b) -> {
                    BigInteger combinedResult = a.add(b);
                    System.out.println("\t\t>>Calling combiner " + a + " + " + b + " = " + combinedResult);
                    return combinedResult;
                }
            );

        System.out.println("\t\tReducing with accumulator and combiner: " + reduced);
    }


    void intStreams() {
        System.out.println("\n\nintStreams:");

        IntSummaryStatistics stats = countrySet.stream()
                                         .map(Country::getName)
                                         .filter(name -> name.startsWith("M"))
                                         .peek(name -> System.out.println("\t\t\tCountry Name: " + name))
                                         .mapToInt(String::length)
                                         .summaryStatistics();

        System.out.println("\tstats: " + stats);
    }
}
