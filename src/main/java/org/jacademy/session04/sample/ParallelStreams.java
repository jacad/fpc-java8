package org.jacademy.session04.sample;

import org.jacademy.model.Country;

import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static java.lang.String.format;

public class ParallelStreams {

    public static void main(String... args) {
        ParallelStreams p = new ParallelStreams();
        p.parallelStreamsInsights();
        p.parallelIssue();
        //p.parallelWorkaround();
    }

    private static final String WORKER_PREFIX = "ForkJoinPool.commonPool-worker-";
    private static final String SEPARATOR = "|";
    private static final String REGEX = "\\" + SEPARATOR;
    private static final String NONE = "none";
    private static final String MAIN = "main";

    private AtomicInteger accumulatorCounter = new AtomicInteger(0);
    private AtomicInteger combinerCounter = new AtomicInteger(0);

    private Consumer<Country> renameThreads = country -> {
        Thread thread = Thread.currentThread();
        String name = thread.getName();
        if (name.startsWith(WORKER_PREFIX) || name.equals(MAIN)) {
            name = name.replace(WORKER_PREFIX, "T")
                       .replaceAll(MAIN, "T0");
            thread.setName(name);
        }
    };

    Set<Country> getCountrySet() {
        return Stream.of(Locale.getAvailableLocales())
                   .filter(locale -> !locale.getDisplayCountry().isEmpty() || !locale.getCountry().isEmpty())
                   .map(locale -> new Country(locale.getDisplayCountry(), locale.getCountry()))
                   .collect(Collectors.toSet());
    }

    void parallelStreamsInsights() {
        long start = System.currentTimeMillis();
        String result = getCountrySet()
            .stream()
            .parallel()
            .peek(renameThreads)
            .map(Country::getName)
            .reduce(
                "none|0",
                this::accumulate,
                this::combine
            );

        long time = System.currentTimeMillis() - start;

        System.out.printf("\t\tReducing with accumulator and combiner: [%s] with an elapsed time of %d\n", result, time);
    }

    private String accumulate(String source, String name) {
        String[] values = source.split(REGEX);
        int acc = Integer.parseInt(values[1]) + name.length();
        String lbl = NONE.equals(values[0])
                         ? format("ACC%02d", accumulatorCounter.incrementAndGet())
                         : values[0];
        String result =  format("%s|%04d", lbl, acc);
        System.out.printf("%s ::\t%s\n", Thread.currentThread().getName(), result);
        return result;
    }

    private String combine(String a, String b) {
        String[] aValues = a.split(REGEX);
        String[] bValues = b.split(REGEX);

        String lbl = aValues[0].contains("COM")
                         ? aValues[0]
                         : format("COM%02d", combinerCounter.incrementAndGet());

        int com = Integer.parseInt(aValues[1]) + Integer.parseInt(bValues[1]);

        String result = format("%s|%04d", lbl, com);
        System.out.printf("%s ::\t\t\t\t%s\t\t =\t\t%s + %s\n", Thread.currentThread().getName(), result, a, b);
        return result;
    }

    private void parallelIssue() {
        long start = System.currentTimeMillis();
        IntStream.range(0, 32)
            .parallel()
            //.peek(renameThreads)
            .map(n -> {
                System.out.printf("%s :: Outter Map [%d]\n", Thread.currentThread().getName(), n);
                return Math.abs(n);
            })
            .peek(n -> innerEvenParallelStream(n, 25))
            .peek(n -> innerOddParallelStream(n, 50))
            .peek(this::innerAllParallelStream)
            .count();

        long time = System.currentTimeMillis() - start;

        System.out.printf("\t\tElapsed time (Parallel with inner parallels) Fixed %d\n", time);

    }

    private void parallelWorkaround() {
        long start = System.currentTimeMillis();
        IntStream.range(0, 32)
            .parallel()
            //.peek(renameThreads)
            .map(n -> {
                System.out.printf("%s :: Outter Map (Fixed) [%d]\n", Thread.currentThread().getName(), n);
                return Math.abs(n);
            })
            .peek(n -> innerEvenParallelStreamFixed(n, 25))
            .peek(n -> innerOddParallelStreamFixed(n, 50))
            .peek(this::innerAllParallelStreamFixed)
            .count();

        long time = System.currentTimeMillis() - start;

        System.out.printf("\t\tElapsed time (Parallel with inner parallels) Fixed %d\n", time);

    }

    private int innerEvenParallelStream(int number, int factor) {
        LongStream.range(0, number)
            .filter(n -> n % 2 == 0)
            .parallel()
            .forEach(n -> {
                System.out.printf("\t\t\t\t%s :: Inner Even [%d:%d]\n", Thread.currentThread().getName(), number, n);
                sleep(n, factor);
            });

        return 0;
    }

    private int innerOddParallelStream(int number, int factor) {
        LongStream.range(0, number)
            .filter(n -> n % 2 == 1)
            .parallel()
            .forEach(n -> {
                System.out.printf("\t\t\t\t%s :: Inner odd [%d:%d]\n", Thread.currentThread().getName(), number, n);
                sleep(n, factor);
            });

        return 0;
    }

    private int innerAllParallelStream(int number) {
        LongStream.range(0, number)
            .parallel()
            .forEach(n ->
                System.out.printf("\t\t\t\t%s :: Inner All [%d:%d]\n", Thread.currentThread().getName(), number, n)
        );

        return 0;
    }

    private void sleep(long n, long factor) {
        try {
            TimeUnit.MILLISECONDS.sleep(n * factor);
        } catch (InterruptedException e) {
            //do nothing
        }
    }

    private int innerEvenParallelStreamFixed(int n, int factor) {
        ForkJoinPool forkJoinPool = new ForkJoinPool(2);
        try {
            return forkJoinPool.submit(() -> innerEvenParallelStream(n, factor)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return 1;
    }

    private int innerOddParallelStreamFixed(int n, int factor) {
        ForkJoinPool forkJoinPool = new ForkJoinPool(3);
        try {
            return forkJoinPool.submit(() -> innerOddParallelStream(n, factor)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return 1;
    }

    private int innerAllParallelStreamFixed(int n) {
        ForkJoinPool forkJoinPool = new ForkJoinPool(2);
        try {
            return forkJoinPool.submit(() -> innerAllParallelStream(n)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return 1;
    }
}
