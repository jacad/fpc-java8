package org.jacademy.session04.exercise;

import org.jacademy.model.Person;
import org.jacademy.model.shop.Purchase;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Set;

public class Exercise {
    //From a list of purchases, retrieve the DoubleSummaryStatistics for the prices, avoid duplicated
    // Discard null purchases and details, and prices equals or lesser than zero
    public DoubleSummaryStatistics productPriceStats(List<Purchase> purchases) {
        return null;
    }

    //From a list of Persons, calculate the Age stats
    //discard null persons or without birthday registered, also discard if the birthday is invalid (greater than today)
    //
    public LongSummaryStatistics ageStats(List<Person> persons) {
        return null;
    }

    //Receiving a list of persons, group them by Address Country Code
    //The container map must be ordered and contain no duplicated elements and restrict null persons.
    //Note: any element in the address section, and the address itself, may be null; any of those, should be under "N/A"
    //The same for country code that are empty
    public Map<String, Set<Person>> groupByCountry(List<Person> persons) {
        return null;
    }
}
