package org.jacademy.model;

import java.util.Objects;

public class Country {
    private String name;
    private String isoCode;

    public Country(String name, String isoCode) {
        //System.out.println("\tCreating a new country " + name);
        this.name = name;
        this.isoCode = isoCode;
    }

    public Country(String name) {
        this.name = name;
        System.out.println("instancing country");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    @Override
    public String toString() {
        return "Country{" +
                   "name='" + name + '\'' +
                   ", isoCode='" + isoCode + '\'' +
                   '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return Objects.equals(name, country.name) &&
                   Objects.equals(isoCode, country.isoCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, isoCode);
    }
}
