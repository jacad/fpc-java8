package org.jacademy.model;

import java.time.LocalDate;
import java.util.Objects;

public class Person {

    public enum Sex {Male, Female}

    private String name;
    private String lastName;
    private LocalDate birthday;
    private Sex sex;
    private Address address;

    public Person(String name, LocalDate birthday, Sex sex) {
        this.name = name;
        this.birthday = birthday;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                   Objects.equals(birthday, person.birthday) &&
                   sex == person.sex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birthday, sex);
    }

    @Override
    public String toString() {
        return "Person{" +
                   "name='" + name + '\'' +
                   ", lastName='" + lastName + '\'' +
                   ", address=" + address +
                   '}';
    }
}
