package org.jacademy.model;

public class Address {
    private String line1;
    private String line2;
    private City city;
    private String zipCode;

    public Address(String line1, String line2, City city, String zipCode) {
        this.line1 = line1;
        this.line2 = line2;
        this.city = city;
        this.zipCode = zipCode;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }
}
