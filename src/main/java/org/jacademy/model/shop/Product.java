package org.jacademy.model.shop;

import java.util.Objects;

public class Product {

    public enum ProductType {
        Medicine, Food, Liqueur, Tobacco, Other
    }

    private String code;
    private String name;
    private double price;
    private ProductType type;

    public Product(String code, String name, double price, ProductType type) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 &&
                   Objects.equals(code, product.code) &&
                   Objects.equals(name, product.name) &&
                   type == product.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, price, type);
    }

    @Override
    public String toString() {
        return "Product{" +
                   "code='" + code + '\'' +
                   ", name='" + name + '\'' +
                   ", price=" + price +
                   ", type=" + type +
                   '}';
    }
}
