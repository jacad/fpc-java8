package org.jacademy.model.shop;

public class PurchaseDetail {
    private Product product;
    private Double quantity;

    public PurchaseDetail(Product product, double quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "PurchaseDetail{" +
                   "product=" + product +
                   ", quantity=" + quantity +
                   '}';
    }
}
