package org.jacademy.sesion01.sample;

import org.junit.Test;

public class LambdaScopeTest {

    @Test
    public void closuresScopes() {
        LambdaScopesSample closuresSample = new LambdaScopesSample();

        closuresSample.lambdaScopes();
    }

    @Test
    public void closuresWithSideEffectScopes() {
        LambdaScopesSample closuresSample = new LambdaScopesSample();

        closuresSample.lambdaWithSideEffects();
    }
}