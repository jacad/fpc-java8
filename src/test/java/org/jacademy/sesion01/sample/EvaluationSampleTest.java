package org.jacademy.sesion01.sample;

import org.jacademy.model.City;
import org.jacademy.model.Country;
import org.jacademy.model.State;
import org.junit.Test;

public class EvaluationSampleTest {

    private static final City GDL = new City("Guadalajara", new State("Jalisco", new Country("Mexico")));
    private static final City UNKNOWN = new City("n/a", null);

    private final EvaluationSample evaluationSample = new EvaluationSample();

    @Test
    public void getCountryEager() {
        evaluationSample.getCountryEager(GDL);
        //evaluationSample.getCountryEager(UNKNOWN);
    }

    @Test
    public void getCountryLazy() {
        evaluationSample.getCountryLazy(GDL);
        //evaluationSample.getCountryLazy(UNKNOWN);
    }
}