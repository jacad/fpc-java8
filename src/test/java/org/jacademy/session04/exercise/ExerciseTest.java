package org.jacademy.session04.exercise;

import org.jacademy.model.Address;
import org.jacademy.model.City;
import org.jacademy.model.Country;
import org.jacademy.model.Person;
import org.jacademy.model.State;
import org.jacademy.model.shop.Product;
import org.jacademy.model.shop.Purchase;
import org.jacademy.model.shop.PurchaseDetail;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.isNull;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.jacademy.model.shop.Product.ProductType.Food;
import static org.jacademy.model.shop.Product.ProductType.Liqueur;
import static org.jacademy.model.shop.Product.ProductType.Medicine;
import static org.jacademy.model.shop.Product.ProductType.Other;
import static org.jacademy.model.shop.Product.ProductType.Tobacco;
import static org.junit.Assert.*;

public class ExerciseTest {

    Exercise sut = new Exercise();

    @Test
    public void productPriceStats() {
        DoubleSummaryStatistics priceStats = sut.productPriceStats(generate());

        assertThat(priceStats.getAverage(), closeTo(5.9563, .0001));
        assertThat(priceStats.getSum(), closeTo(47.65, .0001));
        assertThat(priceStats.getMax(), closeTo(9.16, .0001));
        assertThat(priceStats.getMin(), closeTo(1.5, .0001));
        assertThat(priceStats.getCount(), is(8L));
    }
    private List<Purchase> generate() {
        Product p1 = new Product("001", "Prod01",2.0, Food);
        Product p2 = new Product("002", "Prod02",1.5, Liqueur);
        Product p3 = new Product("003", "Prod03",4.89, Tobacco);
        Product p4 = new Product("004", "Prod04",5.89, Medicine);
        Product p5 = new Product("005", "Prod05",8.16, Other);
        Product p6 = new Product("006", "Prod06",8.89, Medicine);
        Product p7 = new Product("007", "Prod07",9.16, null);
        Product p8 = new Product("008", "Prod08",0.0, Food);
        Product p9 = new Product("009", "Prod09",7.16, Food);
        Product p10 = new Product("010", "Prod10",-12.45, Food);

        PurchaseDetail d1 = new PurchaseDetail(p1, 10.0);
        PurchaseDetail d2 = new PurchaseDetail(p2, 5.5);
        PurchaseDetail d3 = new PurchaseDetail(p3, 6.6);
        PurchaseDetail d4 = new PurchaseDetail(p4, 7.7);
        PurchaseDetail d5 = new PurchaseDetail(p5, 8.8);
        PurchaseDetail d6 = new PurchaseDetail(p6, 9.9);
        PurchaseDetail d7 = new PurchaseDetail(p7, 12.1);
        PurchaseDetail d8 = new PurchaseDetail(p8, 12.3);
        PurchaseDetail d9 = new PurchaseDetail(p9, 13.4);
        PurchaseDetail d10 = new PurchaseDetail(p10, 15.6);
        PurchaseDetail d11 = new PurchaseDetail(p2, 24.0);
        PurchaseDetail d12 = new PurchaseDetail(p3, 35.5);
        PurchaseDetail d13 = new PurchaseDetail(p5, 36.6);
        PurchaseDetail d14 = new PurchaseDetail(p7, 47.7);
        PurchaseDetail d15 = new PurchaseDetail(p9, 38.8);
        PurchaseDetail d16 = new PurchaseDetail(null, 38.8);
        PurchaseDetail d17 = new PurchaseDetail(p1, 0.0);
        PurchaseDetail d18 = new PurchaseDetail(p1, -12.56);

        List<Purchase> purchases = new ArrayList<>();
        purchases.add(new Purchase());
        purchases.add(new Purchase());
        purchases.add(new Purchase());
        purchases.add(new Purchase());
        purchases.add(null);

        purchases.get(0).addDetail(d1, d3, d5, null, d7, d8, d13, d16);
        purchases.get(1).addDetail(d2, d4, d5, d6, d10, d14, d15, d18);
        purchases.get(2).addDetail(d9, d11, d12, d7, null, d3, d4, d15, d17);

        return purchases;
    }

    @Test
    public void ageStats() {
        LongSummaryStatistics actual = sut.ageStats(generatePersons());

        assertThat(actual.getAverage(), closeTo(40.3333, .0001));
        assertThat(actual.getSum(), is(121L));
        assertThat(actual.getMax(), is(73L));
        assertThat(actual.getMin(), is(10L));
        assertThat(actual.getCount(), is(3L));
    }

    List<Person> generatePersons() {
        Country mx = new Country("Mexico", "MX");
        State mxState = new State("State", mx);
        City mxCity = new City("City", mxState);

        City mxCity2 = new City("City 2", mxState);

        Country us = new Country("USA", "US");
        State usState = new State("State", us);
        City usCity = new City("City", usState);

        City c1 = new City("", new State("", new Country("", "")));


        LocalDate now = LocalDate.now();
        Person p1 = new Person("Person 1", now.minusMonths(125), Person.Sex.Male);
        p1.setAddress(new Address("l1", "l2", mxCity, "l"));

        Person p2 = new Person("Person 2", now.minusMonths(456), Person.Sex.Female);
        p2.setAddress(new Address("l1", "l2", mxCity2, "l"));

        Person p3 = new Person("Person 3", now.minusMonths(876), Person.Sex.Male);
        p3.setAddress(new Address("l1", "l2", usCity, "l"));

        Person p4 = new Person("Person 4", null, Person.Sex.Female);
        p4.setAddress(new Address("l1", "l2", new City("", null), "l"));

        Person p5 = new Person("Person 5", now.plusYears(22), Person.Sex.Female);
        p5.setAddress(new Address("l1", "l2", null, "l"));

        Person p6 = new Person("Person 5", null, Person.Sex.Female);
        p6.setAddress(new Address("l1", "l2", c1, "l"));

        return Arrays.asList(p1, p6, p3, p4, null, p3, p5, p5, p6, p2);
    }

    @Test
    public void groupByCountry() {
        Map<String, Set<Person>> actual = sut.groupByCountry(generatePersons());

        assertThat(actual.size(), is(3));
        assertThat(actual.get("MX").size(), is(2));
        assertThat(actual.get("US").size(), is(1));
        assertThat(actual.get("N/A").size(), is(3));
    }
}