package org.jacademy.session02;

import org.jacademy.model.shop.Product;
import org.jacademy.model.shop.PurchaseDetail;

import static java.util.Objects.isNull;

public class TestUtils {
    private TestUtils() {
    }

    public static PurchaseDetail buildDetail(Double quantity, Double price, Product.ProductType type) {
        if (isNull(quantity)) {
            return null;
        }
        Product product = isNull(price)
                              ? null
                              : buildProduct(price, type);
        return new PurchaseDetail(product, quantity);
    }

    private static Product buildProduct(Double price, Product.ProductType type) {
        return new Product("abc", "ABC", price, type);
    }
}
