package org.jacademy.session02.exercise;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.jacademy.model.Person;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.util.function.Function;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.jacademy.model.Person.Sex.Female;
import static org.jacademy.model.Person.Sex.Male;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class Exercise01Test {

    private Exercise01 sut = new Exercise01();

    @Test
    public void verifyFinalVelocityCurriedFunction() {
        try {
            Function<Double, Function<Double, Double>> fp1 = sut.finalVelocity.apply(1.5);
            Function<Double, Double> fp2 = fp1.apply(2.4);
            Double result = fp2.apply(0.25);

            assertThat(result, is(2.1));
        } catch (Exception e) {
            fail("The function is not well curried");
        }
    }

    @Test
    public void verifyCalculateDiscountFunction() {
        try {
            Function<Double, Double> fp1 = sut.calculateDiscount.apply(1225.45);
            Double result = fp1.apply(0.25);

            assertThat(result, closeTo(919.09, 0.009));
        } catch (Exception e) {
            fail("The function is not well curried");
        }
    }

    @Test
    @UseDataProvider("getTestData")
    public void verifyStringToPerson(String name, String birthday, String sex, Person expectedPerson) {
        try {
            Function<String, Function<String, Person>> fp1 = sut.convertStrToPerson.apply(name);
            Function<String, Person> fp2 = fp1.apply(birthday);
            Person person = fp2.apply(sex);

            assertThat(person, is(expectedPerson));
        } catch (Exception e) {
            fail("The function is not well curried");
        }
    }

    @DataProvider
    public static Object[][] getTestData() {
        return new Object[][] {
            {"Juan Perez", "1985-06-22", "Male", buildPerson("Juan Perez", LocalDate.parse("1985-06-22"), Male)},
            {"Juan Perez", "wrong-date", "Male", buildPerson("Juan Perez", null, Male)},
            {"Juan Perez", null, "wrongsex", buildPerson("Juan Perez", null, null)},
            {"Juana Perez", null, "Female", buildPerson("Juana Perez", null, Female)}
        };
    }

    private static Object buildPerson(String name, LocalDate birthday, Person.Sex sex) {
        return new Person(name, birthday, sex);
    }
}