package org.jacademy.session02.exercise;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.jacademy.model.Address;
import org.jacademy.model.City;
import org.jacademy.model.Country;
import org.jacademy.model.Person;
import org.jacademy.model.State;
import org.jacademy.model.shop.PurchaseDetail;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.util.Objects;

import static java.lang.Double.NaN;
import static java.lang.Double.isNaN;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.jacademy.model.shop.Product.ProductType.Food;
import static org.jacademy.model.shop.Product.ProductType.Liqueur;
import static org.jacademy.model.shop.Product.ProductType.Tobacco;
import static org.jacademy.session02.TestUtils.buildDetail;
import static org.junit.Assert.assertTrue;

@RunWith(DataProviderRunner.class)
public class Exercise02Test {

    private Exercise02 sut = new Exercise02();

    @Test
    @UseDataProvider("getDisplayNameInTestData")
    public void verifyLocaleTranslation(Person person, String expectedResult) {
        String actual = sut.getDisplayNameIn(person);

        assertThat(actual, is(expectedResult));
    }

    @DataProvider
    public static Object[][] getDisplayNameInTestData() {
        return new Object[][]{
            {buildPerson(true, true, true, true, "MX"), "Spanish"},
            {buildPerson(true, true, true, true, "US"), "English"},
            {buildPerson(true, true, true, true, null), "Not available"},
            {buildPerson(true, true, true, false, "US"), "Not available"},
            {buildPerson(true, true, false, false, "US"), "Not available"},
            {buildPerson(true, false, false, false, "US"), "Not available"},
            {buildPerson(false, false, false, false, "US"), "Not available"},
        };
    }

    private static Person buildPerson(
            boolean withPerson,
            boolean withAddress,
            boolean withCity,
            boolean withState,
            String countryIsoCode) {

        if (!withPerson) {
            return null;
        }

        Country country = Objects.nonNull(countryIsoCode) ? new Country("country", countryIsoCode) : null;
        State state = withState ? new State("testState", country) : null;
        City city = withCity ? new City("testCity", state) : null;
        Address address = withAddress ? new Address("abc", "ABC", city, "12345") : null;

        Person person = new Person("any person", null, Person.Sex.Female);
        person.setAddress(address);
        return person;
    }

    @Test
    @UseDataProvider("getAgeTestData")
    public void verifyCalcualteAge(Person person, Long expectedResult) {
        Long actual = sut.calculateAge(person);

        assertThat(actual, is(expectedResult));
    }

    @DataProvider
    public static Object[][] getAgeTestData() {
        return new Object[][]{
            {buildPerson(LocalDate.now(), Person.Sex.Male), 0L},
            {buildPerson(LocalDate.MIN, Person.Sex.Female), 0L},
            {buildPerson(null, Person.Sex.Male), 0L},
            {buildPerson(LocalDate.now().minusYears(17), Person.Sex.Male), 17L},
            {buildPerson(LocalDate.now().minusYears(17), Person.Sex.Female), 0L},
        };
    }

    private static Person buildPerson(LocalDate birthday, Person.Sex sex) {
        return new Person("any person", birthday, sex);
    }

    @Test
    @UseDataProvider("getCalculateTotalTestData")
    public void verifyCalculateTotal(PurchaseDetail p1, PurchaseDetail p2, Double expectedTotal) {
        Double actual = sut.calculateTotal(p1, p2);

        if (isNaN(expectedTotal)) {
            assertTrue("expectedTotal: " + expectedTotal + " but was " + actual, isNaN(actual));
        } else {
            assertThat(actual, closeTo(expectedTotal, 0.009));
        }
    }

    @DataProvider
    public static Object[][] getCalculateTotalTestData() {
        return new Object[][] {
            {buildDetail(2.0, 1.25, Food), buildDetail(1.5, 1.25, Liqueur), 4.79},
            {null, buildDetail(1.5, 1.25, Liqueur), NaN},
            {buildDetail(1.5, 1.25, Liqueur), null, NaN},
            {buildDetail(0.0, 1.25, Food), buildDetail(1.5, 1.25, Liqueur), NaN},
            {buildDetail(3.58, 0.0, Food), buildDetail(1.5, 1.25, Liqueur), NaN},
            {buildDetail(750.25, 2.25, Tobacco), buildDetail(1.0, 0.001, Liqueur), 1908.35}
        };
    }
}