package org.jacademy.session02.sample;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.jacademy.model.shop.Product;
import org.jacademy.model.shop.PurchaseDetail;
import org.junit.Test;
import org.junit.runner.RunWith;

import static java.util.Objects.isNull;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.jacademy.model.shop.Product.ProductType.Food;
import static org.jacademy.model.shop.Product.ProductType.Liqueur;
import static org.jacademy.model.shop.Product.ProductType.Medicine;
import static org.jacademy.model.shop.Product.ProductType.Other;
import static org.jacademy.model.shop.Product.ProductType.Tobacco;

@RunWith(DataProviderRunner.class)
public class MoreOptionalUsagesTest {

    @Test
    @UseDataProvider("getTestData")
    public void verifyTaxCalculation(PurchaseDetail purchaseDetail, String expectedResult) {
        _02d_MoreOptionalUsages sut = new _02d_MoreOptionalUsages();

        String actual = sut.getTaxAsString(purchaseDetail);

        System.out.printf("Actual [%s]", actual);
        assertThat(actual, is(expectedResult));
    }

    @DataProvider
    public static Object[][] getTestData() {
        return new Object[][] {
            {buildDetail(2.0, 1.25, Food), "Tax applied: $ 0.00"},
            {buildDetail(1.5, 1.25, Liqueur), "Tax applied: $ 0.41"},
            {buildDetail(4.89, 2.35, Tobacco), "Tax applied: $ 2.18"},
            {buildDetail(4.89, 2.35, Medicine), "Tax applied: $ 0.00"},
            {buildDetail(7.16, 0.75, Other), "Tax applied: $ 0.86"},
            {buildDetail(4.89, 2.35, Medicine), "Tax applied: $ 0.00"},
            {buildDetail(7.16, 0.75, null), "Tax applied: $ 0.86"},
            {null, "Not enough information to calculate tax."},
            {buildDetail(null, 0.01, Food), "Not enough information to calculate tax."},
            {buildDetail(7.16, null, Food), "Not enough information to calculate tax."},
            {buildDetail(null, null, Food), "Not enough information to calculate tax."}
        };
    }

    private static PurchaseDetail buildDetail(Double quantity, Double price, Product.ProductType type) {
        if (isNull(quantity)) {
            return null;
        }
        Product product = isNull(price)
                              ? null
                              : buildProduct(price, type);
        return new PurchaseDetail(product, quantity);
    }


    private static Product buildProduct(Double price, Product.ProductType type) {
        return new Product("abc", "ABC", price, type);
    }

}