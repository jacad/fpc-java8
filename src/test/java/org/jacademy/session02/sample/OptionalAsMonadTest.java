package org.jacademy.session02.sample;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(DataProviderRunner.class)
public class OptionalAsMonadTest {
    @Test
    @UseDataProvider("getTestData")
    public void verifyOptionalAdd(Integer a, Integer b, Optional<Integer> expectedValue) {
        _02c_OptionalAsMonad sut = new _02c_OptionalAsMonad();

        Optional<Integer> actual = sut.optionalAdd(a, b);

        System.out.printf("Actual [%s]", actual);
        assertThat(actual.isPresent(), is(expectedValue.isPresent()));

        expectedValue.ifPresent(val -> assertThat(expectedValue.get(), is(val)));
    }

    @DataProvider
    public static Object[][] getTestData() {
        return new Object[][] {
            {null, null, Optional.empty()},
            {null, 1, Optional.empty()},
            {2, null, Optional.empty()},
            {2, 1, Optional.of(3)}
        };
    }
}