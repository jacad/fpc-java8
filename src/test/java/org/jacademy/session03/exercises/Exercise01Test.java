package org.jacademy.session03.exercises;

import org.jacademy.model.shop.Product;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.jacademy.model.shop.Product.ProductType.Liqueur;
import static org.jacademy.model.shop.Product.ProductType.Medicine;
import static org.jacademy.model.shop.Product.ProductType.Other;
import static org.jacademy.model.shop.Product.ProductType.Tobacco;

public class Exercise01Test {
    private Exercise01 sut = new Exercise01();


    @Test
    public void verifyNumberProcessing() {
        List<String> numbers = Arrays.asList(
            "234", "1233", "999", "971", "17", "19", "654", "222", "541", "173", "899", "1000", "1971", "667"
        );

        Object[] numbersProcessed = sut.processNumber(numbers);

        assertThat(numbersProcessed, is(new Object[]{173L, 541L, 667L, 899L, 971L}));
    }

    @Test
    public void verifyPalindromes() {
        List<String> words = Arrays.asList(
            "oso", "ana", "selles", "carro", "Reconocer", "enero", "Arenera", "martes", "selles", "Rotomotor", "salero"
        );

        Object[] palindromes = sut.findPalindromes(words);

        assertThat(palindromes, is(new Object[]{"selles", "Arenera", "Reconocer", "Rotomotor"}));
    }

    @Test
    public void verifyProducts() {
        List<String> lines = Arrays.asList(
            "0035,Sal,nine,Medicine",
            "0003,Aspirina,25.50,Medicine",
            "0004,Cuervo Especial,235.67,Liqueur",
            null,
            "0005,Marlboro Rojos,67.89,Tobacco",
            "0011,Coca-Cola Sin Azucar,,Candy",
            ",,,",
            "0066,Chocolate Blanco,17.23,Other",
            "",
            "0001,Coca-Cola,12.30,Other",
            "0002,Chocolate,8.90,Other",
            "0033,Antiacido,25.50,Medicine",
            "0004,Cuervo Especial,235.67,Liqueur"
        );

        Object[] products = sut.readProducts(lines);
        String[] productsStr = new String[products.length];
        for (int i = 0; i < products.length; i++) {
            productsStr[i] = products[i].toString();
        }

        String[] value = {
            new Product("0001","Coca-Cola",12.30, Other).toString(),
            new Product("0002","Chocolate",8.90, Other).toString(),
            new Product("0003","Aspirina",25.50, Medicine).toString(),
            new Product("0004","Cuervo Especial",235.67, Liqueur).toString(),
            new Product("0005","Marlboro Rojos",67.89, Tobacco).toString(),
            new Product("0033","Antiacido",25.50, Medicine).toString(),
            new Product("0066","Chocolate Blanco",17.23, Other).toString()
        };
        assertThat(productsStr, is(value));
    }





}