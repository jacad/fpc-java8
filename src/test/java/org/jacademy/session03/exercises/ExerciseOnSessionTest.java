package org.jacademy.session03.exercises;


import org.jacademy.model.Country;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ExerciseOnSessionTest {
    private ExerciseOnSession sut = new ExerciseOnSession();

    @Test
    public void verifyGeneration() {
        long total = sut.generatedCreationWithLimit(10);

        assertThat(total, is(10L));
    }

    @Test
    public void verifyCountrySorting() {
        List<Country> countries = Arrays.asList(
            new Country("United States", "US"),
            new Country("Mexico", "MX"),
            new Country("Italia", "IT"),
            new Country("Francia", "FR"),
            new Country("Andorra", "AN")
        );

        Object[] actual = sut.sortCountries(countries);

        Object[] expected = new Object[] { "IT - Italia", "MX - Mexico", "AN - Andorra", "FR - Francia", "US - United States"};

        assertThat(actual, is(expected));
    }
}